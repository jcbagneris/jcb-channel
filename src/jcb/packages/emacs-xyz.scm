(define-module (jcb packages emacs-xyz)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix i18n)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module (gnu packages)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages base)
  #:use-module (gnu packages scheme)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages gnupg)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match))

(define-public emacs-denote
  (package
    (name "emacs-denote")
    (version "2.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.sr.ht/~protesilaos/denote")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0nwqghh73lbw6v6yhkalcwwqjs1fyhxqi53d9y2dcfhfq58g1rb9"))))
    (build-system emacs-build-system)
    (native-inputs (list texinfo))
    (home-page "https://protesilaos.com/emacs/denote/")
    (synopsis "Simple notes for Emacs")
    (description
"Denote is a simple note-taking tool for Emacs.  It is based on the idea that
notes should follow a predictable and descriptive file-naming scheme.  The
file name must offer a clear indication of what the note is about, without
reference to any other metadata.  Denote basically streamlines the creation of
such files while providing facilities to link between them.")
    (license license:gpl3+)))

(define-public emacs-denote-latest
  (let ((commit "1a2bc831bea487a8c597aff68a902e397bcb21b0")
        (revision "1"))
  (package
   (inherit emacs-denote)
   (name "emacs-denote-latest")
   (version (git-version "2.1.0-dev" revision commit))
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://git.sr.ht/~protesilaos/denote")
           (commit commit)))
     (file-name (git-file-name name version))
     (sha256
      (base32 "1v8shh8zb76qjq1qagvazr31n3gx075h8fgpj8nlj07hjbp2ij9a"))))
   (description
"Denote is a simple note-taking tool for Emacs.  It is based on the idea that
notes should follow a predictable and descriptive file-naming scheme.  The
file name must offer a clear indication of what the note is about, without
reference to any other metadata.  Denote basically streamlines the creation of
such files while providing facilities to link between them.

This package aims to follow and test the tip of the development branch.
Most users would prefer to use the `emacs-denote' package."))))

(define-public emacs-citar-denote
  (let ((commit "2107142e3c621aa64f95c7820d4b12b69f1763f4")
        (revision "1"))
  (package
   (name "emacs-citar-denote")
   (version (git-version "1.3.0-dev" revision commit))
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/pprevos/citar-denote.git")
           (commit commit)))
     (file-name (git-file-name name version))
     (sha256
      (base32 "1x82y75w5g84vjdvxl5xvlbrlph40rm9fg0cblixq12i4sdjb9ks"))))
    (build-system emacs-build-system)
    (native-inputs (list emacs-denote-latest emacs-dash emacs-citar))
    (home-page "https://lucidmanager.org/productivity/bibliographic-notes-in-emacs-with-citar-denote/")
    (synopsis "Literature notes for denote")
   (description
    "The `citar-denote' auxiliary package integrates the `citar' bibliography package with the `denote' note-taking system. `citar-denote' provides a global minor mode that enables linking notes to a bibliographic entry. Each bibliography item can have one or more notes, and each note can relate to one or more pieces of literature. The reference line in the front matter of your notes contains the citation keys (separated by a semicolon), and each bibliographic note is marked with the _bib file tag.")
   (license license:gpl3))))

(define-public emacs-mistty
  (let ((commit "5d411da307c899ad0756cef7e9c203bb8efe8615")
        (revision "1"))
  (package
   (name "emacs-mistty")
   (version (git-version "0.9-dev" revision commit))
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/szermatt/mistty.git")
           (commit commit)))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0sv9rr2hg8fzgl9pqbg6g9xql4szsy7981q8lhqg6kl8v9rljphs"))))
    (build-system emacs-build-system)
    (native-inputs (list texinfo))
    (home-page "https://mistty.readthedocs.io/en/latest/index.html")
    (synopsis "MisTTY, a shell/comint alternative with a fully functional terminal.")
   (description "MisTTY is a major mode for Emacs 29.1 and up that runs a shell inside of a buffer, similarly to comint mode. It is built on top of term.el. Check out its project page at https://github.com/szermatt/mistty.")
   (license license:gpl3))))
